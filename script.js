var can1, can2, can3, can4;
var ctx1, ctx2, ctx3, ctx4;
var first, second,third,four;

function init(){
    can1 = document.getElementById("canvas1");
    can2 = document.getElementById("canvas2");
    can3 = document.getElementById("canvas3");
    can4 = document.getElementById("canvas4");
    can5 = document.getElementById("canvas5");

    can1.width = can2.width = can3.width = can4.width = can5.width = 400;
    can1.height = can2.height = can3.height = can4.height = can5.height = 400;

    ctx1 = can1.getContext('2d');
    ctx2 = can2.getContext('2d');
    ctx3 = can3.getContext('2d');
    ctx4 = can4.getContext('2d');
    ctx5 = can5.getContext('2d');

    first = new SimpleFigure(ctx1,1,8,1,1.66,5,5.66,5,8,3,6);
    second = new SimpleFigure(ctx2,1,10,9,18,5,18,5,19.66,1,15.66,4,16);
    third = new SimpleFigure(ctx3,13,4,17,8,13,8,14.3,6.6);
    four = new SimpleFigure(ctx4,13,10,17,14,17,18.68,13,18.68,15,16);
  
    first.Paint();
    second.Paint();
    third.Paint();
    four.Paint();
    //console.log(first.GetDots());
    //CrossFigure(first,second);


    first.Center();
    first.Turn(1);
    first.Paint();
    first.Turn(1);
    first.Paint();


    second.Center();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();
    second.Turn(1);
    second.Paint();

    third.Center();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();
    third.Turn(1);
    third.Paint();

    four.Center();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
    four.Turn(1);
    four.Paint();
}