var SIZE = 10;

//класс создания точки
function createDot(x, y){
    var x = x;
    var y = y;
    
    this.X = function(X){
        if (!arguments.length) return x;
        x = X;
    }
    this.Y = function(Y){
        if (!arguments.length) return y;
        y = Y;
    }

}  
//класс фигруы, входные параментры по порядку:
//(context,x0,y0,...,xn,yn,xc,yc)
//context холст, на котором рисуем
//x0,y0,...,xn,yn - координаты многоугольника 
//xc,yc - координаты центра вращения фигуры
function SimpleFigure(context){
    var Dots = [];
    var width = context.canvas.width, height = context.canvas.height;

    if(arguments.length%2==0 || arguments.length == 0){
        throw Error("Не верное количество параметров\n\targuments.length = " + arguments.length);
    }
    
    var xC = arguments[arguments.length - 2] * SIZE - width/4, yC = arguments[arguments.length - 1] * SIZE - height/4;
    for (var i = 1; i < arguments.length - 2; i+=2) {
        Dots.push(new createDot(arguments[i] * SIZE - width/4, arguments[i+1] * SIZE - height/4));
    };
    Dots.push(new createDot(arguments[1] * SIZE- width/4, arguments[2] * SIZE - height/4));
    context.translate(width/2, height/2);

    //рисование точки
    this.Point = function( steep, x, y, a){
        if (!steep){
            context.fillStyle = "rgba("+0+","+0+","+0+","+a+")"; 
            context.fillRect(x,y,1,1);
        } else {
            context.fillStyle = "rgba("+0+","+0+","+0+","+a+")"; 
            context.fillRect(y,x,1,1);
        }
    }
    //рисование линии
    this.Line = function (x0, y0, x1, y1){
        var steep = Math.abs(y1 - y0) > Math.abs(x1 - x0);
        if (steep){
            var temp = x0;
            x0 = y0;
            y0 = temp;
            temp = x1;
            x1 = y1;
            y1 = temp;
        }
        if (x0 > x1){
            var temp = x1;
            x1 = x0;
            x0 = temp;
            temp = y0;
            y0 = y1;
            y1 = temp;
        }

        this.Point(steep, x0, y0, 1);
        var dx = x1 - x0;
        var dy = y1 - y0;
        var gradient = dy / dx;
        var y = y0 + gradient;
        for (var x = x0 + 1; x <= x1 - 1; x++)
        {
            this.Point(steep, x, Math.floor(y), (1 - (y - Math.floor(y))));
            this.Point(steep, x, Math.floor(y) + 1, (y - Math.floor(y)));
            y += gradient;
        }
        this.Point(steep, x1, y1, 1);
    }
    //рисование фигуры
    this.Paint = function (){
        for (var i = 0; i < Dots.length - 1; i++) {
            this.Line(Dots[i].X(),Dots[i].Y(),Dots[i+1].X(),Dots[i+1].Y());
        };
    } 
    //поворот координат
    //а - в зависимости от знака а будет движение по часовой или против часовой стрелки
    this.Turn = function (a){
        var temp1,temp2;        
        for (var i = 0; i < Dots.length; i++) {         
            temp1 = (Dots[i].X() - xC)*Math.cos(Math.PI/4) - Math.sign(a) * (Dots[i].Y() - yC)*Math.sin(Math.PI/4) + xC;
            temp2 = Math.sign(a) * (Dots[i].X() - xC)*Math.sin(Math.PI/4) + (Dots[i].Y() - yC)*Math.cos(Math.PI/4) + yC;
            Dots[i].X(temp1);
            Dots[i].Y(temp2);   
        }        
    }
    //рисование центра вращения
    this.Center = function (){
        this.Point(0,xC,yC,1);
    }
    //получение точек объекта
    this.GetDots = function(){
        return Dots;
    }    
    //отражение фигуры относительно оси ординат
    this.Reflection = function(){
        for (var i = 0; i < Dots.length; i++) {         
            Dots[i].X(-Dots[i].X() + 2 * xC);
        } 
    }
}

//класс для проверки пересечения фигур
//входящие параметры - массив вершин фигур
//внутри имеется метод пересечения отрезков
function IntersectFigures(first, second){  
    //метод пересечения линий
    this.IntersectLines = function(x1, y1, x2, y2, x3, y3, x4, y4) {
        var dx1 = x2 - x1;
        var dy1 = y2 - y1;
        var dx2 = x4 - x3;
        var dy2 = y4 - y3;
        var x = dy1 * dx2 - dy2 * dx1;
        if(!x || !dx2)
            return false;
        var y = x3 * y4 - y3 * x4;
        x = ((x1 * y2 - y1 * x2) * dx2 - y * dx1) / x;
        y = (dy2 * x - y) / dx2;
        return ((x1 < x && x2 > x) || (x2 < x && x1 > x)) && ((x3 < x && x4 > x) || (x4 < x && x3 > x));
    }   

    var intersect = false;

    var f = first;
    var s = second;

    for (var i = 0; i < f.length - 1; i++) {
        for (var j = 0; j < s.length - 1; j++) {

                intersect = this.IntersectLines(f[i].X(),f[i].Y(),f[i + 1].X(),f[i + 1].Y(),s[j].X(),s[j].Y(),s[j + 1].X(),s[j + 1].Y());
//                console.log(this.IntersectLines(f[i].X(),f[i].Y(),f[i + 1].X(),f[i + 1].Y(),s[j].X(),s[j].Y(),s[j + 1].X(),s[j + 1].Y()));
//                console.log("x0 = " + f[i].X() + " y0 = " + f[i].Y() + " x1 = " + f[i + 1].X() + " y1 = " + f[i + 1].Y() + "\nx0 = " + s[j].X() + " y0 = " +  s[j].Y() + " x1 = " + s[j + 1].X() + " y1 = " + s[j + 1].Y());
                if(intersect) return true;
        };        
    };        
    return false;
}

//Пересечение фигур между собой
function IntersectFiguresTogether(figures){
    for (var i = 0; i < figures.length - 1; i++) {
        for (var j = i + 1; j < figures.length; j++) {
            if(IntersectFigures(figures[i].GetDots(),figures[j].GetDots())){
                document.getElementById("par").textContent = "Фигуры пересекаются";
                return;
            }
            else{
                document.getElementById("par").textContent = "Фигуры не пересекаются";
            }
        };
    };
}